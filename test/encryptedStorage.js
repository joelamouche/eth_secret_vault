const EncryptedStorage = artifacts.require("./EncryptedStorage.sol");
const {expectRevert, expectEvent}=require('@openzeppelin/test-helpers')
const web3Utils=require('web3-utils')

contract("Encrypted Storage", accounts => {
  const creator=accounts[0]
  const goodUser=accounts[1]
  const badUser=accounts[2]
  let EncryptedStorageInstance;
  let testValue;
  let secretName
  let secretValue

  before('setup contract',async()=>{
    EncryptedStorageInstance = await EncryptedStorage.deployed();
    testValue=await web3Utils.asciiToHex('testValue')
    secretName=await web3Utils.asciiToHex('secret name')
    secretValue=await web3Utils.asciiToHex('secret value')
  })

  // User key Management

  it("...should store the value 89.", async () => {
    // Set value of 89
    await EncryptedStorageInstance.setUserKey(creator,testValue,testValue, { from: creator });

    // Get stored value
    const storedData = await EncryptedStorageInstance.getUserKey(creator)

    assert.equal(storedData[0], testValue, "The value 89 was not stored.");
  });

  it("...should store a long string.", async () => {
    const longString=await web3Utils.asciiToHex('qerherhatrhstjsrtjsrtjrtjtrsnesrnstrhsgsfhsrjstrjdtjdty666NNNN986876e21rsjrsjrjytdsgnstrjtrmhgdmgfnbgfnhtkrlyullllllrhgrwrhweuturjwrueytkjdtykdtykdtykdtkdtykdtykykytd')
    
    // Set value of 89
    await EncryptedStorageInstance.setUserKey(creator,longString,longString, { from: creator });

    // Get stored value
    const storedData = await EncryptedStorageInstance.getUserKey(creator)

    assert.equal(storedData[0], longString, "The long string was not stored.");
  });

  it("...should prevent another user from setting the value", async () => {
    // Set value of 89
    await expectRevert(EncryptedStorageInstance.setUserKey(creator,testValue,testValue, { from: badUser }),'Caller is not an authorized user of the vault');
  });

  it("...should add a good user", async () => {
    // Set value of 89
    await EncryptedStorageInstance.setUserKey(goodUser,testValue,testValue, { from: creator })
    const storedData = await EncryptedStorageInstance.getUserKey(goodUser)
    assert.equal(storedData[0], testValue, "The value 89 was not stored.");
  });

  it("...should prevent another user from setting the value after good user is authorized", async () => {
    // Set value of 89
    await expectRevert(EncryptedStorageInstance.setUserKey(goodUser,testValue,testValue, { from: badUser }),'Caller is not an authorized user of the vault');
  });

  it("...should allow the authorized good user to add someone else", async () => {
    // Auth bad user
    await EncryptedStorageInstance.setUserKey(badUser,testValue,testValue, { from: goodUser })
    const storedData = await EncryptedStorageInstance.getUserKey(badUser)
    assert.equal(storedData[0], testValue, "The value 89 was not stored.");
  });

  it("...should allow the authorized good user to add someone else and then remove them", async () => {
    // Remove bad user
    await EncryptedStorageInstance.removeUser(badUser, { from: goodUser })
    const storedData = await EncryptedStorageInstance.getUserKey(badUser)
    assert.equal(storedData[0], null, "The bad user wasnt removed.");
  });
  
  it("...should return the right events for user addition and removal", async () => {
    // Auth bad user
    let {logs} = await EncryptedStorageInstance.setUserKey(badUser,testValue,testValue, { from: goodUser })
    expectEvent.inLogs(logs, 'UserAdded', {
      userAddress:badUser,
      userKey:testValue
    });
    // Remove bad user
    let res = await EncryptedStorageInstance.removeUser(badUser, { from: goodUser })
    expectEvent.inLogs(res.logs, 'UserRemoved', {
      userAddress:badUser
    });
  });

  // Secret management

  it("...should store a secret.", async () => {
    // Set value of 89
    await EncryptedStorageInstance.setSecret(secretName,secretValue, { from: creator });

    // Get stored value
    const storedData = await EncryptedStorageInstance.getSecret(secretName)

    assert.equal(storedData, secretValue, "The value 89 was not stored.");
  });

  it("...should store a long string.", async () => {
    const longString=await web3Utils.asciiToHex('qerherhatrhstjsrtjsrtjrtjtrsnesrnstrhsgsfhsrjstrjdtjdty666NNNN986876e21rsjrsjrjytdsgnstrjtrmhgdmgfnbgfnhtkrlyullllllrhgrwrhweuturjwrueytkjdtykdtykdtykdtkdtykdtykykytd')
    
    // Set value of 89
    await EncryptedStorageInstance.setSecret(secretName,longString, { from: creator });

    // Get stored value
    const storedData = await EncryptedStorageInstance.getSecret(secretName)

    assert.equal(storedData, longString, "The long string was not stored.");
  });

  it("...should prevent another user from setting the value", async () => {
    // Set value of 89
    await expectRevert(EncryptedStorageInstance.setSecret(secretName,secretValue, { from: badUser }),'Caller is not an authorized user of the vault');
  });

  it("...should prevent another user from setting the value after good user is authorized", async () => {
    await EncryptedStorageInstance.setUserKey(goodUser,testValue,testValue, { from: creator })
    await expectRevert(EncryptedStorageInstance.setSecret(secretName,secretValue, { from: badUser }),'Caller is not an authorized user of the vault');
  });

  it("...should allow the authorized good user to add a secret", async () => {
    // Auth bad user
    await EncryptedStorageInstance.setSecret(secretName,secretValue, { from: goodUser })
    const storedData = await EncryptedStorageInstance.getSecret(secretName)
    assert.equal(storedData, secretValue, "The value 89 was not stored.");
  });
  
  it("...should return the right events for user addition and removal", async () => {
    // Auth bad user
    let {logs} = await EncryptedStorageInstance.setSecret(secretName,secretValue, { from: goodUser })
    expectEvent.inLogs(logs, 'SecretAdded', {
      secretName:secretName,
      secretValue:secretValue
    });
  });
});
