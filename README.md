# eth_secret_vault

An Ethereum-backed DApp that implements a key vault working in a secure and decentralized way.

## Challenge instructions

[Instructions for this challenge in this markdown file](key_vault.md)

## Install instructions

Contracts

1. First install the truffle repo

`npm i`

2. Then, run the ganache test chain

`npm run testchain`

3. Migrate the contracts

`npm run migrate`

4. Test the contracts

`npm run test`

Client

5. Install

`cd client`

`npm i`

6. Run tests

`npm run test`

7. Run the App

`npm run start`

8. Connect to local host on your browser and use MetaMask to use the app

## Notes on the solution

### On storing keys in the browser
https://crypto.stackexchange.com/questions/35530/where-and-how-to-store-private-keys-in-web-applications-for-private-messaging-wi
https://www.nccgroup.com/us/about-us/newsroom-and-events/blog/2011/august/javascript-cryptography-considered-harmful/

- Use Web Cryptography API
https://pomcor.com/2017/06/02/keys-in-browser/
"In first approximation, this means that if a string is stored by JavaScript code embedded in a web page downloaded from a particular DNS domain, it can only be retrieved by JavaScript code that is also embedded in a web page downloaded from the same DNS domain."

- Javascript client side crypto is not very safe

### On MetaMask Key Storage
https://www.reddit.com/r/ethereum/comments/dor0ou/how_metamask_backs_up_wallet_data_with_3box/
"Private data must be consented by the user in order for the frontend to decrypt and read the data. Users consent by signing a message from their web3 wallet/provider. Decryption keys are generated from the consent signature."

### On MetaMask Encryption
https://github.com/MetaMask/metamask-extension/issues/1190#issuecomment-369777513
"curve25519 is much nicer curve for encryption, and the nacl is a world class library (the js version tweet-nacl is also security reviewed by cure58 and very well tested)
Since the secp256k1 private keys are just random numbers these can be used as curve25519 private keys as well
Thus, to each ETH address we can associate a curve25519 public key (computed from the secp256k1 private key) that can be used for encryption with the nacl library.
This is another way to do “web3 encryption” that I like more"

### On 3Box ethereum based encryption
https://medium.com/3box/3box-architecture-a3e35c82e919

"The basic way that Ethereum wallets can support 3ID database authentication is by supporting the personal_sign method in the standard EthereumProvider API. When apps call openBox() or openSpace() in the 3Box API to gain access to a user's database(s), the request is transported over the Ethereum JSON-RPC interface to the wallet which displays a personal_sign request to the user. If this request is approved, the message is signed with the user's Ethereum private key and the resulting signature is returned to the application over Ethereum JSON-RPC. The combination of the user's unique private key and message is the material which is used to generate a 3ID and additional database keys, which are then stored in localStorage and used by the app to perform database operations."

## Proposed solutions for encrypting text with an ETH id

All the solutions above derive an encryption key from a signature from metaMask, so as not to expose the MetaMask private key.

Those solution rely on the curve25519 asymmetric encryption scheme, which is supported by forge (ed25519) but the original and most popular implementation seems to be TweetNacl.
We are in a situation where Metamask is a black box that can sign messages but not expose private key.
In all those cases, a new user need to communicate to the contract deployer their curve25519 public key. These could be stored on the contract associated with addresses or in a 3box space.

### 1. Derive curve25519 private key from a string signature
We could have MetaMask sign a constant string and use this result as the private key of the curve25519 encryption scheme.
The advantage is that we can always derive encryption key from MetaMask alone without storing information elsewhere.
The disadvantage is that if the user was tricked into signing the same constant string again, the encryption would be exposed.

### 2. Store curve25519 private key in 3Box or Web Cryptography API
3box is an encrypted storage based on MetaMask id. So it is already a solution to this problem so it feels like cheating.
The advantage is that it is seamless and secure.
The disadvantage is that it relies on a 3rd party storage service.

We could use the Web Cryptography API, as explained in the first link provided, but I chose not to spend time looking at this for now.

### 3. Use an ethereum based encryption library and create our own wallet

https://github.com/pubkey/eth-crypto is a library that claims to implement asymmetric encryption for ethereum.

In order for this library to work, we need to create a new key pair, and expose the private key before we save it into MetaMask (no trivial).

Ideally, I would recommend creating an isolated middleware to manage keys and sign/encrypt/decrypt in a black box manner. This requires more engineering efforts though.

## Conclusion

Given the time and knowledge in my hands, I will go with solution number 1 and derive NaCl encryption key from eth signature.

This exposes some security concerns:
- MetaMask itself could be compromised so I would recommend backing it up with a cold wallet, and changing key vault often
- If the user accidentally signs the message on another platform, they expose their NaCl private key. Moreover, the NaCl encryption key is a truncation of the signature, so this might facilitate key breaking. We could also mitigate this by changing the signed string often but this would require to interact securely with a server and to re-encrypt all the secrets.

- Due to the design of the proposed solution, if the common shared key is expose once, all secrets are exposed. It would be safer, and more expensive, to encrypt the secret for each authorized user's public key.


