<html>
      <h1 class="mume-header" id="jarvis-blockchain-challenge">Jarvis blockchain challenge</h1>

<h2 class="mume-header" id="multi-user-blockchain-persisted-key-vault-br-on-chain-storage-of-off-chain-encrypted-data-br-draft-rev-1">Multi-user blockchain-persisted key vault <br> /On-chain storage of off-chain encrypted data/ <br> [DRAFT rev. 1]</h2>

<div class="tags">
<span>Tags:</span>
<ul>
<li>Security</li>
<li>Cryptography</li>
<li>DApps</li>
<li>Ethereum</li>
<li>Solidity</li>
<li>Unit testing</li>
<li>Node.js</li>
<li>TypeScript</li>
</ul>
</div>
<blockquote>
<h3>Glossary</h3>
<p>In the context of this challenge, we define the meaning of some terms in the following way:</p>
<ul>
<li><strong>DApp</strong> - an open-source application, whose backend operates autonomously, by virtue of its code and data is cryptographically stored in a public, decentralized peer-to-peer network (e.g. blockchain), usually in the form of a smart contract.</li>
<li><strong>key vault</strong> - a system which allows <strong>secrets</strong> to be stored securely</li>
<li><strong>secret</strong> - any sensitive data, represented by <code>name: value</code> pair</li>
<li><strong>decentralized system</strong> - a system (e.g. backed by smart contract) that can operate without intervention by any centralized authority (including its original developers). Individual participants can own separate instances of the system, or participants can share one instance, but no single participant can have more power over that instance than others.</li>
<li><strong>secure</strong> - for all participants <code>A</code> and <code>B</code> (<code>A</code> &#x2260; <code>B</code>), all <strong>secrets</strong> stored by participant <code>A</code> in a <strong>key vault</strong> must not be feasibly accessible as plain-text, unless <code>A</code> <strong>authorized</strong> <code>B</code></li>
<li><strong>authorize</strong> - when one participant grants one or more rights over a system instance to another user</li>
</ul>
</blockquote>
<h3 class="mume-header" id="challenge-overview">Challenge Overview</h3>

<p>Create an Ethereum-backed <strong>DApp</strong> that implements a <strong>key vault</strong> working in a <strong>secure</strong> and <strong>decentralized</strong> way.</p>
<p>The DApp should include two parts:</p>
<ol>
<li>
<p><strong>Off-chain web client</strong> - written in <code>TypeScript</code>, using <code>web3.js</code> and a UI framework of your choice (or even plain HTML), that facilitates:</p>
<ul>
<li>key vault deployment (one smart contract per user)</li>
<li>adding, listing and removing of users that control the key vault</li>
<li>adding (encryption), listing, reading (decryption) and removing of secrets</li>
</ul>
<p>Use either <a href="https://metamask.github.io/metamask-docs/">MetaMask</a> directly, or <a href="https://www.npmjs.com/package/@openzeppelin/network">openzeppelin-network.js</a> (or something similar) as the basis of the user&apos;s identity</p>
</li>
<li>
<p><strong>On-chain backend</strong> - smart contract (one instance deployed per each user) that implements the key vault</p>
<ul>
<li><code>N:N</code> Multi-signature support
<ul>
<li>initially, the user that deployed the smart contract is the only authorized user</li>
<li>each authorized user can add new users and remove existing ones (including himself)</li>
<li>list authorized users</li>
</ul>
</li>
<li>Upgradable
<ul>
<li>The the SC implementation should be replaceable by authorized users</li>
<li>Code and data should be stored as separate smart contracts to optimize gas usage (i.e. multiple instances can share the same smart contract code)</li>
</ul>
</li>
<li>Key vault storage
<ul>
<li>Per-user copy of shared secret (see below)</li>
<li>Store an enumerable mapping of secrets (<code>secretName</code> -&gt; <code>secretValue</code>; <code>secret_names[]</code>)</li>
</ul>
</li>
<li>(Optional) Meta-data
<ul>
<li>Git repo URL and commit hash corresponding to the version of the source code used to build the DApp</li>
<li>(bonus; see <a href="#bonus-tasks">below</a>) IPFS/Swarm hash of the <strong>Off-chain</strong> part</li>
<li>Address of previous (if such exists) version of the smart contract after upgrade</li>
</ul>
</li>
</ul>
</li>
</ol>
<h3 class="mume-header" id="proposed-key-vault-implementation">Proposed key vault implementation</h3>

<ul>
<li>On smart-contract deployment:
<ol>
<li>a new symmetric encryption key, called <code>sharedKey</code>, is generated</li>
<li>it&apos;s encrypted with the user&apos;s public key (off-chain) <code>encryptedSharedKey = encrypt(publicKey, sharedKey)</code></li>
<li>it&apos;s stored in a <code>mapping(address =&gt; bytes) userKeys</code> mapping on-chain: <code>userKeys[address] = encryptedSharedKey</code>, where <code>address</code> is the address that is deploying the smart contract</li>
</ol>
</li>
<li>The <code>sharedKey</code> can be retrieved back by an authorized user <code>A</code> by:
<ol>
<li>reading <code>userKeys[address]</code>, where <code>address</code> is user <code>A</code>&apos;s address</li>
<li>decrypting the <code>encryptedSharedKey</code> with user <code>A</code>&apos;s public key</li>
</ol>
</li>
<li>An existing user can <strong>authorize</strong> a new user by:
<ol>
<li>(optionally) retrieving the <code>sharedKey</code></li>
<li>encrypting the <code>sharedKey</code> with the new user&apos;s public key off-chain</li>
<li>adding it to the <code>userKeys</code> mapping</li>
</ol>
</li>
<li>Secrets are added by:
<ol>
<li>(optionally) retrieving the <code>sharedKey</code></li>
<li>encrypting the <code>secretValue</code> with the <code>sharedKey</code> off-chain</li>
<li>persisting them on the blockchain (<code>secrets[secretName] = encryptedSecretValue</code>)</li>
</ol>
</li>
<li>Secrets are retrieved by:
<ol>
<li>(optionally) retrieving the <code>sharedKey</code></li>
<li>reading them: <code>secrets[secretName]</code></li>
<li>decrypting them with the <code>sharedKey</code> off-chain</li>
</ol>
</li>
</ul>
<p>Below is a pseudo code implementation of the client-side of the protocol.<br>
It&apos;s incomplete only for illustrative purposes. It is your job to do a real implementation in TypeScript.</p>
<pre data-role="codeBlock" data-info="js" class="language-javascript"><span class="token comment">// Pseudo code. Implementation details (such choice of symmetric encryption</span>
<span class="token comment">// algorithm and mode) are intentionally left out as an exercise to the</span>
<span class="token comment">// reader/implementer.</span>

<span class="token keyword">function</span> <span class="token function">createKeyVaultInstance</span><span class="token punctuation">(</span><span class="token parameter">keyVaultContract<span class="token punctuation">,</span> firstUserPublicKey<span class="token punctuation">,</span> firstUserPrivateKey</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">const</span> sharedKey <span class="token operator">=</span> <span class="token function">generateSymmetricEncryptionKey</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> encryptedSharedKey <span class="token operator">=</span> <span class="token function">encryptWithPublicKey</span><span class="token punctuation">(</span>firstUserPublicKey<span class="token punctuation">,</span> sharedKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> contractInstance <span class="token operator">=</span> <span class="token keyword">await</span> keyVaultContract
        <span class="token punctuation">.</span><span class="token function">deployInstance</span><span class="token punctuation">(</span><span class="token punctuation">{</span> args<span class="token punctuation">:</span> <span class="token punctuation">[</span>encryptedSharedKey<span class="token punctuation">]</span> <span class="token punctuation">}</span><span class="token punctuation">)</span>
        <span class="token punctuation">.</span><span class="token function">send</span><span class="token punctuation">(</span>firstUserPrivateKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">return</span> contractInstance<span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token keyword">function</span> <span class="token function">addUser</span><span class="token punctuation">(</span><span class="token parameter">newUserPublicKey<span class="token punctuation">,</span> contractInstance<span class="token punctuation">,</span> existingUserAddress<span class="token punctuation">,</span> existingUserPrivateKey</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">const</span> encryptedSharedKey <span class="token operator">=</span> <span class="token keyword">await</span> contractInstance
        <span class="token punctuation">.</span><span class="token function">getSharedKeyForAddress</span><span class="token punctuation">(</span>existingUserAddress<span class="token punctuation">)</span>
        <span class="token punctuation">.</span><span class="token function">send</span><span class="token punctuation">(</span>existingUserPrivateKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> sharedKey <span class="token operator">=</span> <span class="token function">decryptWithPrivateKey</span><span class="token punctuation">(</span>existingUserPrivateKey<span class="token punctuation">,</span> encryptedSharedKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> newUserEncryptedSharedKey <span class="token operator">=</span> <span class="token function">encryptWithPublicKey</span><span class="token punctuation">(</span>newUserPublicKey<span class="token punctuation">,</span> sharedKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> newUserAddress <span class="token operator">=</span> newUserPublicKey<span class="token punctuation">.</span><span class="token function">toAddress</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token comment">// must fail if address exists already:</span>
    <span class="token keyword">await</span> contractInstance<span class="token punctuation">.</span><span class="token function">addUserKey</span><span class="token punctuation">(</span>newUserAddress<span class="token punctuation">,</span> newUserEncryptedSharedKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token keyword">function</span> <span class="token function">addSecret</span><span class="token punctuation">(</span><span class="token parameter">secretName<span class="token punctuation">,</span> secretValue<span class="token punctuation">,</span> contractInstance<span class="token punctuation">,</span> myPublicAddress<span class="token punctuation">,</span> myPrivateKey</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">const</span> encryptedSharedKey <span class="token operator">=</span> <span class="token keyword">await</span> contractInstance<span class="token punctuation">.</span><span class="token function">getSharedKeyForAddress</span><span class="token punctuation">(</span>myPublicAddress<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">send</span><span class="token punctuation">(</span>myPrivateKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> sharedKey <span class="token operator">=</span> <span class="token function">decryptWithPrivateKey</span><span class="token punctuation">(</span>myPrivateKey<span class="token punctuation">,</span> encryptedSharedKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> encryptedSecret <span class="token operator">=</span> <span class="token function">symmetricEncrypt</span><span class="token punctuation">(</span>sharedKey<span class="token punctuation">,</span> secretValue<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">await</span> contractInstance
        <span class="token punctuation">.</span><span class="token function">setSecret</span><span class="token punctuation">(</span>secretName<span class="token punctuation">,</span> encryptedSecret<span class="token punctuation">)</span>
        <span class="token punctuation">.</span><span class="token function">send</span><span class="token punctuation">(</span>myPrivateKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token keyword">function</span> <span class="token function">getSecret</span><span class="token punctuation">(</span><span class="token parameter">secretName<span class="token punctuation">,</span> contractInstance<span class="token punctuation">,</span> myPublicAddress<span class="token punctuation">,</span> myPrivateKey</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">const</span> encryptedSharedKey <span class="token operator">=</span> <span class="token keyword">await</span> contractInstance<span class="token punctuation">.</span><span class="token function">getSharedKeyForAddress</span><span class="token punctuation">(</span>myPublicAddress<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">send</span><span class="token punctuation">(</span>myPrivateKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> sharedKey <span class="token operator">=</span> <span class="token function">decryptWithPrivateKey</span><span class="token punctuation">(</span>myPrivateKey<span class="token punctuation">,</span> encryptedSharedKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> encryptedSecret <span class="token operator">=</span> <span class="token keyword">await</span> contractInstance<span class="token punctuation">.</span><span class="token function">getSecret</span><span class="token punctuation">(</span>secretName<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">send</span><span class="token punctuation">(</span>myPrivateKey<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">return</span> secret <span class="token operator">=</span> <span class="token function">symmetricDecrypt</span><span class="token punctuation">(</span>sharedKey<span class="token punctuation">,</span> encryptedSecret<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</pre><h3 class="mume-header" id="research-and-development">Research and development</h3>

<ul>
<li>Analyze the protocol proposed above. Do you see any security flows in it? If so, can you propose a better one?</li>
<li>Implement this or better protocol.</li>
<li>Write unit tests for both the client and the smart contract.</li>
</ul>
<h3 class="mume-header" id="bonus-tasks">Bonus tasks</h3>

<ul>
<li>Deploy the off-chain web client part of the DApp to IPFS (or Swarm) and store the hash identifier of the front-end to the smart contract</li>
<li>Allow user to upgrade the web client, by setting a new the IPFS/Swarm hash in the smart contract and doing a client-side redirect when the hash is read and if it is different than the currently loaded front-end.</li>
</ul>