var SimpleStorage = artifacts.require("./SimpleStorage.sol");
var EncryptedStorage = artifacts.require("./EncryptedStorage.sol");


module.exports = function(deployer) {
  deployer.deploy(SimpleStorage);
  deployer.deploy(EncryptedStorage);
};
