import {expect} from 'chai';
import {CryptoJSEncryptor, ForgeEncryptor, TweetNaclWeb3Encryptor} from '../src/utils/cryptoUtils'
import Web3 from "web3";
import nacl, {BoxKeyPair, box, secretbox, randomBytes} from 'tweetnacl'
import {
  decodeUTF8,
  encodeUTF8,
  encodeBase64,
  decodeBase64
} from "tweetnacl-util";


const textToEncrypt:string="Some serious stuff"

describe("Encryption libraries", () => {
  describe("CryptoJSEncryptor", () => {
    it("...encrypt and decrypt a simple text", async () => {
      let encryptor:CryptoJSEncryptor=new CryptoJSEncryptor("secret key") 
      let encryptedMessage:string = encryptor.encrypt(textToEncrypt)

      expect(encryptedMessage!==textToEncrypt).to.be.true;
      expect(encryptor.decrypt(encryptedMessage)===textToEncrypt).to.be.true;
    });
  });
  describe("ForgeEncryptor", () => {
    it("...encrypt and decrypt a simple text", async () => {
      let encryptor:ForgeEncryptor=new ForgeEncryptor("secret key") 
      let encryptedMessage:string = encryptor.encrypt(textToEncrypt)

      expect(encryptedMessage!==textToEncrypt).to.be.true;
      expect(encryptor.decrypt(encryptedMessage)===textToEncrypt).to.be.true;
    });
    it("...encrypt and decrypt a simple text for another user's public key", async () => {
      let encryptor1:ForgeEncryptor=new ForgeEncryptor("secret key") 
      let encryptor2:ForgeEncryptor=new ForgeEncryptor("") 

      let encryptedMessage:string = encryptor1.encryptWithPublicKey(textToEncrypt,encryptor2.keyPair.publicKey)

      expect(encryptedMessage!==textToEncrypt).to.be.true;
      expect(encryptor2.decrypt(encryptedMessage)===textToEncrypt).to.be.true;
    });
  });
  describe("TweetNaclWeb3Encryptor", () => {
    let encryptor1:TweetNaclWeb3Encryptor
    let encryptor2:TweetNaclWeb3Encryptor
    before('setup',async()=>{
      const web3:Web3 = new Web3('http://127.0.0.1:8545');
      encryptor1=new TweetNaclWeb3Encryptor(web3)
      await encryptor1.initialize(0)
      encryptor2=new TweetNaclWeb3Encryptor(web3)
      await encryptor2.initialize(1)
    })
    it("...encrypt and decrypt a simple text with asymmetric encryption", async () => {
      const encrypted:string = encryptor1.encryptForPublicKey(textToEncrypt, encryptor2.getPublicKey());
      const decrypted = encryptor2.decryptFromPublicKey(encrypted, encryptor1.getPublicKey());
      expect(textToEncrypt===decrypted).to.be.true;
    });
    it("...encrypt and decrypt a simple text with symmetric encryption", async () => {
      const key:string=encodeBase64(randomBytes(secretbox.keyLength));
      const encrypted:string = TweetNaclWeb3Encryptor.encryptSym(textToEncrypt, key);
      const decrypted = TweetNaclWeb3Encryptor.decryptSym(encrypted, key);
      expect(textToEncrypt===decrypted).to.be.true;
    });
  });
});
