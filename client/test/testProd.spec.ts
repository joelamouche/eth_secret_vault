import {expect} from 'chai';
import {ForgeEncryptor, TweetNaclWeb3Encryptor} from '../src/utils/cryptoUtils'
import EncryptedStorageContract from "../src/contracts/EncryptedStorage.json";
import Web3 from "web3";
import { encodeBase64 } from 'tweetnacl-util';
import { randomBytes, secretbox } from 'tweetnacl';

const secretName:string="THE secret"
const secretMessage:string="Some serious stuff"

const secretName2:string="THE OTHER secret"
const secretMessage2:string="Jujujul"

describe("Encryption libraries and Smart Contract integrations", () => {
  let key:string
  it("...encrypt and decrypt a simple text, with Smart Contract as intermediary", async () => {
    //get web3
    const web3:Web3 = new Web3('http://127.0.0.1:8545');
    const accounts:string[] = await web3.eth.getAccounts();
    const creatorAddress=accounts[0]

    // Get the contract instance.
    const networkId = await web3.eth.net.getId();
    const deployedNetwork = EncryptedStorageContract.networks[networkId];
    //@ts-ignore
    let abi:AbiItem=EncryptedStorageContract.abi
    const contract = new web3.eth.Contract(
      abi,
      deployedNetwork && deployedNetwork.address,
    );

    //step 1:encrypt
    let encryptor:ForgeEncryptor=new ForgeEncryptor("secret key") 
    let encryptedMessage:string =  encryptor.encrypt(secretMessage)
    let hexEncryptedMessage=await web3.utils.asciiToHex(encryptedMessage)
    
    //step 2: store on sc
    // TODO: type contract methods with truffle typings
    //@ts-ignore
    await contract.methods.setUserKey(creatorAddress, hexEncryptedMessage, hexEncryptedMessage).send({from:accounts[0], gas:3000000});

    // Step3: Get the value from the contract to prove it worked.
    //@ts-ignore
    const response:string = await contract.methods.getUserKey(creatorAddress).call();

    //step 4 : decrypt it
    const asciiResp=await web3.utils.hexToAscii(response[0])
    const decryptedValue:string=encryptor.decrypt(asciiResp)


    expect(encryptedMessage!==secretMessage).to.be.true;
    expect(decryptedValue===secretMessage).to.be.true;
  });
  it("...encrypt a text for themselves (asymmetric encryption), sc as intermediary", async () => {
    //get web3
    const web3:Web3 = new Web3('http://127.0.0.1:8545');
    const accounts:string[] = await web3.eth.getAccounts();

    // setup encryptors for both users
    let encryptor1:TweetNaclWeb3Encryptor=new TweetNaclWeb3Encryptor(web3) 
    await encryptor1.initialize(0)

    // Get the contract instance.
    const networkId = await web3.eth.net.getId();
    const deployedNetwork = EncryptedStorageContract.networks[networkId];
    //@ts-ignore
    let abi:AbiItem=EncryptedStorageContract.abi
    const contract = new web3.eth.Contract(
      abi,
      deployedNetwork && deployedNetwork.address,
    );

    //step 1:encrypt
    const encryptedMessage:string = encryptor1.encryptForPublicKey(secretMessage, encryptor1.getPublicKey());
    let hexEncryptedMessage=await web3.utils.asciiToHex(encryptedMessage)
    let hexPublicKey1=await web3.utils.asciiToHex(encryptor1.getPublicKey())
    
    //step 2: store on sc
    // TODO: type contract methods with truffle typings
    //@ts-ignore
    await contract.methods.setUserKey(accounts[0], hexEncryptedMessage, hexPublicKey1).send({from:accounts[0], gas:3000000});

    // Step3: Get the value from the contract to prove it worked.
    //@ts-ignore
    const response:string = await contract.methods.getUserKey(accounts[0]).call({from:accounts[0]});

    //step 4 : decrypt it with user2's encryptor
    const asciiMessage=await web3.utils.hexToAscii(response[0])
    const asciiPublicKey=await web3.utils.hexToAscii(response[1])
    const decryptedValue:string=encryptor1.decryptFromPublicKey(asciiMessage,asciiPublicKey)


    expect(encryptedMessage!==secretMessage).to.be.true;
    expect(decryptedValue===secretMessage).to.be.true;
  });
  it("...encrypt a text for another user (asymmetric encryption), sc as intermediary", async () => {
    //get web3
    const web3:Web3 = new Web3('http://127.0.0.1:8545');
    const accounts:string[] = await web3.eth.getAccounts();

    // setup encryptors for both users
    let encryptor1:TweetNaclWeb3Encryptor=new TweetNaclWeb3Encryptor(web3) 
    await encryptor1.initialize(0)
    let encryptor2:TweetNaclWeb3Encryptor=new TweetNaclWeb3Encryptor(web3) 
    await encryptor2.initialize(1)

    // Get the contract instance.
    const networkId = await web3.eth.net.getId();
    const deployedNetwork = EncryptedStorageContract.networks[networkId];
    //@ts-ignore
    let abi:AbiItem=EncryptedStorageContract.abi
    const contract = new web3.eth.Contract(
      abi,
      deployedNetwork && deployedNetwork.address,
    );

    //step 1:encrypt
    const encryptedMessage:string = encryptor1.encryptForPublicKey(secretMessage, encryptor2.getPublicKey());
    let hexEncryptedMessage=await web3.utils.asciiToHex(encryptedMessage)
    let hexPublicKey1=await web3.utils.asciiToHex(encryptor1.getPublicKey())
    
    //step 2: store on sc
    // TODO: type contract methods with truffle typings
    //@ts-ignore
    await contract.methods.setUserKey(accounts[1], hexEncryptedMessage, hexPublicKey1).send({from:accounts[0], gas:3000000});

    // Step3: Get the value from the contract to prove it worked.
    //@ts-ignore
    const response:string = await contract.methods.getUserKey(accounts[1]).call({from:accounts[1]});

    //step 4 : decrypt it with user2's encryptor
    const asciiMessage=await web3.utils.hexToAscii(response[0])
    const asciiPublicKey=await web3.utils.hexToAscii(response[1])
    const decryptedValue:string=encryptor2.decryptFromPublicKey(asciiMessage,asciiPublicKey)


    expect(encryptedMessage!==secretMessage).to.be.true;
    expect(decryptedValue===secretMessage).to.be.true;
  });
  it("FULL SCENARIO: encrypt a text for another user (asymmetric encryption), sc as intermediary, using an encrypted sharedKey (sym) for the secret and asym encryption for sharedKey", async () => {
    //get web3
    const web3:Web3 = new Web3('http://127.0.0.1:8545');
    const accounts:string[] = await web3.eth.getAccounts();

    // setup encryptors for both users
    let encryptor1:TweetNaclWeb3Encryptor=new TweetNaclWeb3Encryptor(web3) 
    await encryptor1.initialize(0)
    let encryptor2:TweetNaclWeb3Encryptor=new TweetNaclWeb3Encryptor(web3) 
    await encryptor2.initialize(1)

    // Get the contract instance.
    const networkId = await web3.eth.net.getId();
    const deployedNetwork = EncryptedStorageContract.networks[networkId];
    //@ts-ignore
    let abi:AbiItem=EncryptedStorageContract.abi
    const contract = new web3.eth.Contract(
      abi,
      deployedNetwork && deployedNetwork.address,
    );

    //step 1:generate a shared key and encrypt it for user2
    key=encodeBase64(randomBytes(secretbox.keyLength));
    const encryptedMessage:string = encryptor1.encryptForPublicKey(key, encryptor2.getPublicKey());
    let hexEncryptedMessage=await web3.utils.asciiToHex(encryptedMessage)
    let hexPublicKey1=await web3.utils.asciiToHex(encryptor1.getPublicKey())
    
    //step 2: store shared on sc, encrypted for user 2
    // TODO: type contract methods with truffle typings
    //@ts-ignore
    await contract.methods.setUserKey(accounts[1], hexEncryptedMessage, hexPublicKey1).send({from:accounts[0], gas:3000000});

    // step 3: store secret message on sc, encrypted with the key (sym)
    let encryptedSecret= TweetNaclWeb3Encryptor.encryptSym(secretMessage, key);
    let hexSecretName=await web3.utils.asciiToHex(secretName)
    let hexSecretValue=await web3.utils.asciiToHex(encryptedSecret)
    await contract.methods.setSecret(hexSecretName, hexSecretValue).send({from:accounts[0], gas:3000000});

    // Step 4: User 2 gets the shared key from the contract.
    //@ts-ignore
    const response:string = await contract.methods.getUserKey(accounts[1]).call({from:accounts[1]});

    // step 5 : decrypt it with user2's encryptor
    const asciiMessage=await web3.utils.hexToAscii(response[0])
    const asciiPublicKey=await web3.utils.hexToAscii(response[1])
    const decryptedKey:string=encryptor2.decryptFromPublicKey(asciiMessage,asciiPublicKey)
    
    // Step 6: User 2 gets the secret from the contract.
    //@ts-ignore
    const secret:string = await contract.methods.getSecret(hexSecretName).call({from:accounts[1]});
    const asciiSecret=await web3.utils.hexToAscii(secret)

    // Step 7 : user decrypts secret with key
    const decrypted:string = TweetNaclWeb3Encryptor.decryptSym(asciiSecret, decryptedKey);
    expect(decrypted===secretMessage).to.be.true;
  });
  it("also, add the key for the creator", async () => {
    //get web3
    const web3:Web3 = new Web3('http://127.0.0.1:8545');
    const accounts:string[] = await web3.eth.getAccounts();

    // setup encryptors for user 1
    let encryptor1:TweetNaclWeb3Encryptor=new TweetNaclWeb3Encryptor(web3) 
    await encryptor1.initialize(0)

    // Get the contract instance.
    const networkId = await web3.eth.net.getId();
    const deployedNetwork = EncryptedStorageContract.networks[networkId];
    //@ts-ignore
    let abi:AbiItem=EncryptedStorageContract.abi
    const contract = new web3.eth.Contract(
      abi,
      deployedNetwork && deployedNetwork.address,
    );

    //step 1: use same shared key and encrypt it for themselves
    const encryptedMessage:string = encryptor1.encryptForPublicKey(key, encryptor1.getPublicKey());
    let hexEncryptedMessage=await web3.utils.asciiToHex(encryptedMessage)
    let hexPublicKey1=await web3.utils.asciiToHex(encryptor1.getPublicKey())
    
    //step 2: store shared on sc, encrypted for user 1
    // TODO: type contract methods with truffle typings
    //@ts-ignore
    await contract.methods.setUserKey(accounts[0], hexEncryptedMessage, hexPublicKey1).send({from:accounts[0], gas:3000000});

    // step 3: store another secret message on sc, encrypted with the key (sym)
    let encryptedSecret= TweetNaclWeb3Encryptor.encryptSym(secretMessage2, key);
    let hexSecretName=await web3.utils.asciiToHex(secretName2)
    let hexSecretValue=await web3.utils.asciiToHex(encryptedSecret)
    await contract.methods.setSecret(hexSecretName, hexSecretValue).send({from:accounts[0], gas:3000000});

    // Step 4: Same user gets the shared key from the contract.
    //@ts-ignore
    const response:string = await contract.methods.getUserKey(accounts[0]).call({from:accounts[0]});

    // step 5 : decrypt it with user1's encryptor
    const asciiMessage=await web3.utils.hexToAscii(response[0])
    const asciiPublicKey=await web3.utils.hexToAscii(response[1])
    const decryptedKey:string=encryptor1.decryptFromPublicKey(asciiMessage,asciiPublicKey)
    
    // Step 6: User 2 gets the secret from the contract.
    //@ts-ignore
    const secret:string = await contract.methods.getSecret(hexSecretName).call({from:accounts[0]});
    const asciiSecret=await web3.utils.hexToAscii(secret)

    // Step 7 : user decrypts secret with key
    const decrypted:string = TweetNaclWeb3Encryptor.decryptSym(asciiSecret, decryptedKey);
    expect(decrypted===secretMessage2).to.be.true;
  });
});