import React, { Component } from "react";
import EncryptedStorageContract from "./contracts/EncryptedStorage.json";
import getWeb3 from "./utils/getWeb3";

import "./App.css";
import Web3 from "web3";
import { AbiItem } from "web3-utils";
import { TweetNaclWeb3Encryptor } from "./utils/cryptoUtils";
import { AuthUsers, Secrets, logsToAuthUsers, logsToSecrets } from "./utils/onChainDataUtils";
import AddUserForm from './components/AddUserForm'
import RemoveUserForm from './components/RemoveUserForm'
import UserProfile from "./components/UserProfile";
import AuthUsersTable from "./components/AuthUsersTable";
import SecretsTable from './components/SecretsTable'
import AddSecretForm from './components/AddSecretForm'

export interface UserInfo {
  ethPublicAddress:string;
  naclPublicAddress:string;
  isAuthorized:boolean;
}
interface AppState {
  web3:Web3|null;
  accounts:string[];
  contract:any;
  encryptor:TweetNaclWeb3Encryptor|null;
  storageValue:string;
  encryptedValue:string;
  authUsers:AuthUsers;
  secrets:Secrets;
  userInfo:UserInfo;
  sharedKey:string;
}
class App extends Component {
  state:AppState = {
    web3: null, 
    accounts: [], 
    contract: null, 
    encryptor:null ,
    storageValue: '', 
    encryptedValue:'', 
    authUsers:{} ,
    secrets:{},
    sharedKey:"", 
    userInfo:{ethPublicAddress:"",naclPublicAddress:"",isAuthorized:false} 
  };

  setBlockchainEnv = async () => {
    try {
      // Get network provider and web3 instance.
      const web3:Web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts:string[] = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = EncryptedStorageContract.networks[networkId];
      //@ts-ignore
      let abi:AbiItem=EncryptedStorageContract.abi
      const instance = new web3.eth.Contract(
        abi,
        deployedNetwork && deployedNetwork.address,
      );
      // build encryption instance, derived from web3 signature
      const encryptor:TweetNaclWeb3Encryptor=new TweetNaclWeb3Encryptor(web3)
      await encryptor.initialize()

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({ web3, accounts, contract: instance , encryptor});
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  }
  
  loadContractData = async () => {
    const { contract, web3, accounts, encryptor } = this.state;

    // GET SHAREDKEY
    // Step 1: User gets the shared key from the contract.
    //@ts-ignore
    const response:string = await contract.methods.getUserKey(accounts[0]).call({from:accounts[0]});
    let sharedKey:string
    if (response[0]===null){
      sharedKey="unauthorized user"
    } else {
      // step 2 : decrypt it with encryptor
      const asciiMessage=await web3!.utils.hexToAscii(response[0])
      const asciiPublicKey=await web3!.utils.hexToAscii(response[1])
      sharedKey= encryptor!.decryptFromPublicKey(asciiMessage,asciiPublicKey)
    }

    //fetch events
    let logs=await contract!.getPastEvents("allEvents",{fromBlock:0,toBlock:'latest'})
    // fetch creator
    let creator:string=await contract.methods.creator().call()

    // AUTH USERS
    // process events into state
    let authUsers:AuthUsers=await logsToAuthUsers(logs)
    if (!authUsers[creator]){authUsers[creator]={encryptedKey:"", encryptionKey:""}}

    // SECRETS
    // process secret events into state
    let secrets:Secrets=await logsToSecrets(logs, sharedKey)

    // update state
    this.setState({
      authUsers,
      userInfo:{
        ethPublicAddress:this.state.accounts[0],
        naclPublicAddress:this.state.encryptor!.getPublicKey(),
        isAuthorized:Object.keys(authUsers).includes(this.state.accounts[0])
      },
      secrets,
      sharedKey
    })
  }

  setUserKey=async(userAddress:string,recipientNaclPublicKey:string)=>{
    const {contract, web3, accounts, encryptor} = this.state
    // encrypt the key for the recipient
    const encryptedKey:string = encryptor!.encryptForPublicKey(this.state.sharedKey, recipientNaclPublicKey);
    let hexEncryptedKey=await web3!.utils.asciiToHex(encryptedKey)
    // include Public key
    const hexPublicKey:string=await web3!.utils.asciiToHex(encryptor!.getPublicKey())
    // send to smart contract
    try {
      await contract.methods.setUserKey(userAddress, hexEncryptedKey, hexPublicKey).send({ from: accounts[0], gas:3000000 });
      await this.loadContractData();
    } catch(e){
      console.log('error adding user key : ',e)
    }
  }

  setSecret=async(secretName:string,secretValue:string)=>{
    let {web3, sharedKey, contract, accounts}=this.state
    let encryptedSecret= TweetNaclWeb3Encryptor.encryptSym(secretValue, sharedKey);
    let hexSecretName=await web3!.utils.asciiToHex(secretName)
    let hexSecretValue=await web3!.utils.asciiToHex(encryptedSecret)
    // send to smart contract
    try {
      await contract.methods.setSecret(hexSecretName, hexSecretValue).send({from:accounts[0], gas:3000000});
      await this.loadContractData();
    } catch(e){
      console.log('error adding user key : ',e)
    }

  }
  
  removeUser=async(userAddress:string)=>{
    const {contract, accounts} = this.state
    try {
      await contract.methods.removeUser(userAddress).send({ from: accounts[0], gas:3000000 });
      await this.loadContractData();
    } catch (e){
      console.log('error removing user : ',e)
    }
  }

  componentDidMount = async () => {
    await this.setBlockchainEnv();
  };

  componentDidUpdate = async () => {
    if (this.state.userInfo.ethPublicAddress==="") {
      await this.loadContractData();
    }
  };

  render() {
    const {accounts, authUsers, secrets, encryptor, sharedKey}=this.state
    if (!this.state.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    if (this.state.userInfo.ethPublicAddress==="") {
      return <div>Loading contract data...</div>;
    }
    return (
      <div className="App">
        <h2>Key Vault</h2>
        <UserProfile userInfo={this.state.userInfo} />
        <AddUserForm setUserKey={this.setUserKey.bind(this)} sharedKey={sharedKey} />
        <RemoveUserForm removeUser={this.removeUser.bind(this)} />
        {encryptor? <AuthUsersTable accounts={accounts} authUsers={authUsers} encryptor={encryptor} />:null}
        <SecretsTable secrets={secrets} />
        {sharedKey==="unauthorized user"? <div>Unauthorized users can't add secrets</div>: <AddSecretForm setSecret={this.setSecret.bind(this)} />}
      </div>
    );
  }
}

export default App;
