import React, { Component } from "react";
import { frameStyle, titleStyle } from "../styles";

interface AddSecretFormProps {
    setSecret:any;
}
interface AddSecretFormState {
    secretName:string;
  secretValue:string
}
class AddSecretForm extends Component<AddSecretFormProps,AddSecretFormState> {
  constructor(props) {
    super(props);
    this.state = {secretName: '',secretValue:''};

    this.handleChangeSecretName = this.handleChangeSecretName.bind(this);
    this.handleChangeSecretValue = this.handleChangeSecretValue.bind(this);
    this.setSecret = this.setSecret.bind(this);
  }


  handleChangeSecretName(event) {
    this.setState({secretName: event.target.value});
  }

  handleChangeSecretValue(event) {
    this.setState({secretValue: event.target.value});
  }

  setSecret(){
    this.props.setSecret(this.state.secretName,this.state.secretValue)
  }

  //TODO add address validation
  render() {
    return (
      <div style={frameStyle}>
        <div style={titleStyle}>Add a Secret to the Vault</div>
        <label>Secret Name<input type="text" value={this.state.secretName} onChange={this.handleChangeSecretName} /></label>
        <label>Secret Value<input type="text" value={this.state.secretValue} onChange={this.handleChangeSecretValue} /></label>
        <button onClick={this.setSecret}>Add Secret</button>
      </div>
    );
  }
}

export default AddSecretForm;
