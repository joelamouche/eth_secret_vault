import React, { Component } from "react";
import { frameStyle, titleStyle } from "../styles";

interface AddUserKeyFormProps {
  setUserKey:any;
  sharedKey:string;
}
interface AddUserKeyFormState {
  userAddress:string;
  naclKey:string
}
class AddUserForm extends Component<AddUserKeyFormProps,AddUserKeyFormState> {
  constructor(props) {
    super(props);
    this.state = {userAddress: '',naclKey:''};

    this.handleChangeAddress = this.handleChangeAddress.bind(this);
    this.handleChangeKey = this.handleChangeKey.bind(this);
    this.setUserKey = this.setUserKey.bind(this);
  }


  handleChangeAddress(event) {
    this.setState({userAddress: event.target.value});
  }

  handleChangeKey(event) {
    this.setState({naclKey: event.target.value});
  }

  setUserKey(){
    this.props.setUserKey(this.state.userAddress, this.state.naclKey)
  }

  //TODO add address validation
  render() {
    if (this.props.sharedKey==="") {
      return <div>Loading shared key...</div>;
    }
    return (
      <div style={frameStyle}>
        <div style={titleStyle}>Add a User to the Vault</div>
        <label>User Eth Address<input type="text" value={this.state.userAddress} onChange={this.handleChangeAddress} /></label>
        <label>User Nacl Public Address<input type="text" value={this.state.naclKey} onChange={this.handleChangeKey} /></label>
        <button onClick={this.setUserKey}>Add User</button>
        <div>Shared Nacl key : <div>{this.props.sharedKey}</div></div>
      </div>
    );
  }
}

export default AddUserForm;
