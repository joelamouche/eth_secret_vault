import React, { Component } from "react";
import { frameStyle, labelStyle, titleStyle } from "../styles";
import { AuthUsers } from "../utils/onChainDataUtils";
import { TweetNaclWeb3Encryptor } from "../utils/cryptoUtils";

interface AuthUsersTableProps {
    authUsers:AuthUsers;
    encryptor:TweetNaclWeb3Encryptor;
    accounts:string[];
}
class AuthUsersTable extends Component<AuthUsersTableProps,{}> {

  render() {
    return (
        <div style={frameStyle}>
          <table>
            <caption style={titleStyle}>Authorized users</caption>
            <tbody>
                  <tr key={0}>
                      <td style={labelStyle}>User Address</td>
                      <td style={labelStyle}>Encrypted key</td>
                  </tr>
            {
              (Object.keys(this.props.authUsers)).map((userAddress,i)=>{
                let key:string=""
                if (userAddress===this.props.accounts[0]&&this.props.authUsers[userAddress].encryptedKey!==""){
                  try{
                    key=this.props.encryptor!.decryptFromPublicKey(this.props.authUsers[userAddress].encryptedKey,this.props.authUsers[userAddress].encryptionKey)+' (decrypted)'
                  } catch(e){
                    key=this.props.authUsers[userAddress].encryptedKey.substring(0,30)+'...     [decryption error]'
                    console.log('error',e)
                  }
                } else{
                  key=this.props.authUsers[userAddress].encryptedKey.substring(0,30)+'...'
                }
                return (
                  <tr key={i+1}>
                      <td>{userAddress===this.props.accounts[0]? 'YOURSELF':userAddress}</td>
                      <td>{key}</td>
                  </tr>
                )
              })
            }
            </tbody>
          </table>
        </div>
    );
  }
}

export default AuthUsersTable;
