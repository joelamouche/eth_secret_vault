import React, { Component } from "react";
import { frameStyle, labelStyle, titleStyle } from "../styles";
import { Secrets } from "../utils/onChainDataUtils";

interface SecretsTableProps {
    secrets:Secrets;
}
class SecretsTable extends Component<SecretsTableProps,{}> {

  render() {
    return (
        <div style={frameStyle}>
          <table>
            <caption style={titleStyle}>Secrets</caption>
            <tbody>
                  <tr key={0}>
                      <td style={labelStyle}>Secret Name</td>
                      <td style={labelStyle}>Secret Value</td>
                  </tr>
            {
              (Object.keys(this.props.secrets)).map((secretName,i)=>{
                return (
                  <tr key={i+1}>
                      <td>{secretName}</td>
                      <td>{this.props.secrets[secretName]}</td>
                  </tr>
                )
              })
            }
            </tbody>
          </table>
        </div>
    );
  }
}

export default SecretsTable;
