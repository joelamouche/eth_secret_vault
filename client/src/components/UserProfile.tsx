import React, { Component } from "react";
import { UserInfo } from "../App";
import { frameStyle, labelStyle } from "../styles";

interface UserProfileProps {
    userInfo:UserInfo;
}
class UserProfile extends Component<UserProfileProps,{}> {
  render() {
    return (
      <div style={frameStyle}>
        <table>
         <caption style={{fontWeight:'bold',fontSize:'1.3em'}}>User Profile</caption>
          <tbody>
                  <tr>
                      <td style={labelStyle}>Eth Address : </td>
                      <td>{this.props.userInfo.ethPublicAddress}</td>
                  </tr>
                  <tr>
                      <td style={labelStyle}>User Nacl Public Address : </td>
                      <td>{this.props.userInfo.naclPublicAddress}</td>
                  </tr>
                  <tr>
                      <td style={labelStyle}>{this.props.userInfo.isAuthorized? "Authorized":"Unauthorized"}</td>
                  </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default UserProfile;
