import React, { Component } from "react";
import { frameStyle, titleStyle } from "../styles";

interface RemoveUserFormProps {
    removeUser:any;
}
interface RemoveUserFormState {
  userAddress:string;
}
class RemoveUserForm extends Component<RemoveUserFormProps,RemoveUserFormState> {
  constructor(props) {
    super(props);
    this.state = {userAddress: ''};

    this.handleChangeAddress = this.handleChangeAddress.bind(this);
    this.removeUser = this.removeUser.bind(this);
  }

  handleChangeAddress(event) {
    this.setState({userAddress: event.target.value});
  }

  removeUser(){
    this.props.removeUser(this.state.userAddress)
  }

  //TODO add address validation
  render() {
    return (
      <div style={frameStyle}>
        <div style={titleStyle}>Remove user</div>
        <label>User Address<input type="text" value={this.state.userAddress} onChange={this.handleChangeAddress} /></label>
        <button onClick={this.removeUser}>Remove User</button>
      </div>
    );
  }
}

export default RemoveUserForm;
