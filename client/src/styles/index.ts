export const frameStyle={margin:'1em',padding:'1em', border:'0.1em solid grey', textAlign:'center'} as React.CSSProperties

export const labelStyle={padding:"0.2em",margin:'0.3em', fontWeight:"bold"} as React.CSSProperties

export const titleStyle={fontWeight:'bold',fontSize:'1.2em'} as React.CSSProperties