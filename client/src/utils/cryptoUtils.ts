import CryptoJS  from 'crypto-js'
import forge, { pki } from 'node-forge'
import Web3 from "web3";
import nacl, {BoxKeyPair, box, secretbox, randomBytes} from 'tweetnacl'
import naclUtil from 'tweetnacl-util'

export class CryptoJSEncryptor{
    key:string;
    constructor(key:string){
        this.key=key
    }
    encrypt(text:string):string {
        return CryptoJS.AES.encrypt(text, this.key).toString()
    }
    
    decrypt(encryptedMessage:string):string {
        const bytes  = CryptoJS.AES.decrypt(encryptedMessage, this.key);
        return bytes.toString(CryptoJS.enc.Utf8);
    }
}

var rsa=forge.pki.rsa

export class ForgeEncryptor{
    keyPair:pki.rsa.KeyPair;
    constructor(privateKey:string){
        this.keyPair=rsa.generateKeyPair({bits: 2048, e: 0x10001})
    }
    encrypt(bytes:string):string {
        return this.encryptWithPublicKey(bytes,this.keyPair.publicKey)
    }

    encryptWithPublicKey(bytes:string,publicKey:pki.rsa.PublicKey):string{
        return publicKey.encrypt(bytes)
    }
    
    decrypt(encryptedMessage:string):string {
        return this.keyPair.privateKey.decrypt(encryptedMessage)
    }
}

const newNonce = () => randomBytes(box.nonceLength);
const stringToSign:string="ONLY SIGN THIS IF YOU ARE OPENING THE KEY VAULT"

export class TweetNaclWeb3Encryptor{
    web3:Web3;
    keyPair:BoxKeyPair;

    constructor(web3:Web3){
        this.web3=web3;
        this.keyPair=box.keyPair()
    }

    async initialize(accountNumber?:number){
        let accounts:string[]=await this.web3.eth.getAccounts()
        // first, the user signs the string
        // we use the account number for tests
        let sig:string
        if (accountNumber!==undefined){
            sig=await this.web3.eth.sign(stringToSign,accounts[accountNumber])
        } else {
            // and this with metamask
            sig=await this.web3.eth.personal.sign(stringToSign,accounts[0],'')
        }
        let utf8Sig:Uint8Array =naclUtil.decodeUTF8(sig)
        // truncate the signature and use it as private key
        this.keyPair=nacl.box.keyPair.fromSecretKey(utf8Sig.slice(0,32))
    }
    
    getPublicKey():string {
        return naclUtil.encodeBase64(this.keyPair.publicKey)
    }

    // Asymmetric encryption

    encryptAsym(
        secretOrSharedKey: Uint8Array,
        content: string,
        key?: Uint8Array
        ):string{
        const nonce = newNonce();
        const messageUint8 = naclUtil.decodeUTF8(content);
        const encrypted = key
            ? box(messageUint8, nonce, key, secretOrSharedKey)
            : box.after(messageUint8, nonce, secretOrSharedKey);

        const fullMessage = new Uint8Array(nonce.length + encrypted.length);
        fullMessage.set(nonce);
        fullMessage.set(encrypted, nonce.length);

        const base64FullMessage = naclUtil.encodeBase64(fullMessage);
        return base64FullMessage;
        };
    decryptAsym (
            secretOrSharedKey: Uint8Array,
            messageWithNonce: string,
            key?: Uint8Array
          ):string{
            const messageWithNonceAsUint8Array = naclUtil.decodeBase64(messageWithNonce);
            const nonce = messageWithNonceAsUint8Array.slice(0, box.nonceLength);
            const message = messageWithNonceAsUint8Array.slice(
              box.nonceLength,
              messageWithNonce.length
            );
          
            const decrypted = key
              ? box.open(message, nonce, key, secretOrSharedKey)
              : box.open.after(message, nonce, secretOrSharedKey);
          
            if (!decrypted) {
              throw new Error('Could not decrypt message');
            }
          
            const base64DecryptedMessage = naclUtil.encodeUTF8(decrypted);
            return base64DecryptedMessage;
          };

    encryptForPublicKey(secret:string,publicKey:string):string{
        let uintPublicKey:Uint8Array =naclUtil.decodeBase64(publicKey)
        const sharedKey:Uint8Array = box.before(uintPublicKey, this.keyPair.secretKey);
        return this.encryptAsym(sharedKey,secret)
    }
    
    decryptFromPublicKey(encryptedMessage:string, publicKey:string):string {
        let uintPublicKey:Uint8Array =naclUtil.decodeBase64(publicKey)
        const sharedKey:Uint8Array = box.before(uintPublicKey, this.keyPair.secretKey);
        return this.decryptAsym(sharedKey,encryptedMessage)
    }

    // Symmetric Encryption

    static encryptSym = (message:string, key:string):string => {
        const keyUint8Array:Uint8Array = naclUtil.decodeBase64(key);
      
        const nonce = newNonce();
        const messageUint8 = naclUtil.decodeUTF8(message);
        const box = secretbox(messageUint8, nonce, keyUint8Array);
      
        const fullMessage = new Uint8Array(nonce.length + box.length);
        fullMessage.set(nonce);
        fullMessage.set(box, nonce.length);
      
        const base64FullMessage = naclUtil.encodeBase64(fullMessage);
        return base64FullMessage;
      };
    static  decryptSym = (messageWithNonce:string, key:string) => {
        const keyUint8Array:Uint8Array = naclUtil.decodeBase64(key);
        const messageWithNonceAsUint8Array = naclUtil.decodeBase64(messageWithNonce);
        const nonce = messageWithNonceAsUint8Array.slice(0, secretbox.nonceLength);
        const message = messageWithNonceAsUint8Array.slice(
          secretbox.nonceLength,
          messageWithNonce.length
        );
      
        const decrypted = secretbox.open(message, nonce, keyUint8Array);
      
        if (!decrypted) {
          throw new Error("Could not decrypt message");
        }
      
        const base64DecryptedMessage = naclUtil.encodeUTF8(decrypted);
        return base64DecryptedMessage;
      };
}