import utils from 'web3-utils'
import {TweetNaclWeb3Encryptor} from './cryptoUtils'

export interface AuthUsers {
    [userAddress:string]:{encryptedKey:string, encryptionKey:string};
}
export interface Secrets {
    [secretName:string]:string;
}
// TODO: type logs
export const logsToAuthUsers=async function(logs:any[]):Promise<AuthUsers> {
    const authUsers:AuthUsers={}
    // first sync process with hex values
    logs.sort((logA,logB)=>{
        return logA.blockNumber-logB.blockNumber
    }).map((log)=>{
        if (log.event==="UserAdded"){
            authUsers[log.returnValues.userAddress]={encryptedKey:log.returnValues.userKey,encryptionKey:log.returnValues.encryptionKey}
        } else if (log.event==="UserRemoved"){
            authUsers[log.returnValues.userAddress]={encryptedKey:"removed",encryptionKey:"removed"}
        }
        return;
    })
    // convert to ascii - async process
    await Promise.all(Object.keys(authUsers).map(async(userAddress)=>{
        if (authUsers[userAddress].encryptedKey!=="removed"){
            authUsers[userAddress]={encryptedKey:await utils.hexToAscii(authUsers[userAddress].encryptedKey),encryptionKey:await utils.hexToAscii(authUsers[userAddress].encryptionKey)}
        }
        return ;
    }))
    return authUsers
}
export const logsToSecrets=async function(logs:any[], sharedKey:string):Promise<Secrets> {
    const secrets:Secrets={}
    // first sync process with hex values
    logs.sort((logA,logB)=>{
        return logA.blockNumber-logB.blockNumber
    }).map((log)=>{
        if (log.event==="SecretAdded"){
            secrets[log.returnValues.secretName]=log.returnValues.secretValue
        }
        return;
    })
    // convert to ascii - async process, and also decrypt the secrets
    await Promise.all(Object.keys(secrets).map(async(secretName)=>{
            const encryptedSecret:string=await utils.hexToAscii(secrets[secretName])
            const decodedSecretName:string=await utils.hexToAscii(secretName)
            delete secrets[secretName]
            if ("unauthorized user"===sharedKey){
                secrets[decodedSecretName]='unauthorized user '+encryptedSecret
            } else {
                try{
                    const decrypted:string = TweetNaclWeb3Encryptor.decryptSym(encryptedSecret, sharedKey);
                    secrets[decodedSecretName]=decrypted
                } catch(e){
                    console.log('error while decrypting secret',e)
                    secrets[decodedSecretName]='wasnt able to decrypt '+encryptedSecret
                }
            }
        return ;
    }))
    return secrets
}