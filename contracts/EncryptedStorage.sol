// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.7.0;

contract EncryptedStorage {
  address public creator;
  mapping (address => bytes[2]) public userKeys; //TODO: get rid of useless getter
  mapping (address => bool) public authorizedUsers;
  mapping (bytes => bytes) secrets;

  event UserAdded(address userAddress,bytes userKey, bytes encryptionKey); //TODO get rid of block if we can get it form the event
  event UserRemoved(address userAddress);
  event SecretAdded(bytes secretName, bytes secretValue);
  
  modifier onlyAuthorizedUsers() {
    require (authorizedUsers[msg.sender] , 'Caller is not an authorized user of the vault');
    _;
  }

  constructor () public {
    creator=msg.sender;
    authorizedUsers[msg.sender]=true;
  }

  // user management

  function setUserKey(address user,bytes memory key,bytes memory encryptionKey) public onlyAuthorizedUsers {
    userKeys[user]=[key,encryptionKey];
    authorizedUsers[user]=true;
    emit UserAdded(user, key, encryptionKey);
  }
  
  function removeUser(address user) public onlyAuthorizedUsers {
    delete userKeys[user];
    authorizedUsers[user]=false;
    emit UserRemoved(user);
  }

  function getUserKey(address user) public view returns (bytes memory, bytes memory) {
      return (userKeys[user][0],userKeys[user][1]);
  }

  // secret management

  function setSecret(bytes memory secretName, bytes memory secretValue) public onlyAuthorizedUsers {
    secrets[secretName]=secretValue;
    emit SecretAdded(secretName,secretValue);
  }

  function getSecret(bytes memory secretName) public view returns (bytes memory) {
      return secrets[secretName];
  }
}
